# Based on https://code.qt.io/cgit/pyside/pyside-setup.git/tree/examples/widgetbinding/CMakeLists.txt

# TODO: there are some hardcoded paths

function(PythonBindings bindings_library wrapped_header typesystem_file generated_sources dependencies)
    find_package(Shiboken6 REQUIRED)
    find_package(PySide6 REQUIRED)

    set(shiboken_path "/usr/bin/shiboken6")
    if(NOT EXISTS ${shiboken_path})
        message(FATAL_ERROR "Shiboken executable not found at path: ${shiboken_path}")
    endif()

    # Enable rpaths so that the built shared libraries find their dependencies.
    set(CMAKE_SKIP_BUILD_RPATH FALSE)
    set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
    set(CMAKE_INSTALL_RPATH ${SHIBOKEN_PYTHON_MODULE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
    set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

    # Get the relevant Qt include dirs, to pass them on to shiboken.
    set(INCLUDES "")
    list(APPEND INCLUDES "-I/usr/include/KF6/")

    foreach(DEPENDENCY ${dependencies})
        get_property(DEPENDENCY_INCLUDE_DIRS TARGET "${DEPENDENCY}" PROPERTY INTERFACE_INCLUDE_DIRECTORIES)

        foreach(INCLUDE_DIR ${DEPENDENCY_INCLUDE_DIRS})
            list(APPEND INCLUDES "-I${INCLUDE_DIR}")
        endforeach()
    endforeach()

    # Set up the options to pass to shiboken.
    set(shiboken_options --enable-pyside-extensions
        ${INCLUDES}
        -I${CMAKE_SOURCE_DIR}
        -T${CMAKE_SOURCE_DIR}
        -T${PYSIDE_TYPESYSTEMS}
        --output-directory=${CMAKE_CURRENT_BINARY_DIR})

    set(generated_sources_dependencies ${wrapped_header} ${typesystem_file})

    # Add custom target to run shiboken to generate the binding cpp files.
    add_custom_command(OUTPUT ${generated_sources}
                        COMMAND ${shiboken_path}
                        ${shiboken_options} ${wrapped_header} ${typesystem_file}
                        DEPENDS ${generated_sources_dependencies}
                        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                        COMMENT "Running generator for ${typesystem_file}")

    # Set the cpp files which will be used for the bindings library.
    set(${bindings_library}_sources ${generated_sources})

    # Define and build the bindings library.
    add_library(${bindings_library} SHARED ${${bindings_library}_sources})

    # Apply relevant include and link flags.
    target_include_directories(${bindings_library} PRIVATE ${PYSIDE_PYTHONPATH}/include)
    target_include_directories(${bindings_library} PRIVATE ${SHIBOKEN_PYTHON_INCLUDE_DIRS})
    target_include_directories(${bindings_library} PRIVATE "/usr/include/PySide6/")
    target_include_directories(${bindings_library} PRIVATE "/usr/include/PySide6/QtWidgets/")
    target_include_directories(${bindings_library} PRIVATE "/usr/include/PySide6/QtGui/")
    target_include_directories(${bindings_library} PRIVATE "/usr/include/PySide6/QtCore/")

    foreach(DEPENDENCY ${dependencies})
        target_link_libraries(${bindings_library} PRIVATE "${DEPENDENCY}")
    endforeach()

    target_link_libraries(${bindings_library} PRIVATE Shiboken6::libshiboken)
    target_link_libraries(${bindings_library} PRIVATE PySide6::pyside6)

    # Adjust the name of generated module.
    set_property(TARGET ${bindings_library} PROPERTY PREFIX "")
    set_property(TARGET ${bindings_library} PROPERTY OUTPUT_NAME "${bindings_library}${PYTHON_EXTENSION_SUFFIX}")

    install(TARGETS ${bindings_library}
            LIBRARY DESTINATION ${CMAKE_CURRENT_SOURCE_DIR}
            RUNTIME DESTINATION ${CMAKE_CURRENT_SOURCE_DIR}
            )
endfunction(PythonBindings)
