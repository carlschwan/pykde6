# PyKDE6

Python bindings for KDE Frameworks.

## Building

```cmd
mkdir build
cd build
cmake ..
make
```
